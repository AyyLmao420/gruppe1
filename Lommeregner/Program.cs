﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myConsoleMenu
{
    /// <summary>
    /// test summary
    /// </summary>
    class Program
    {
        // detter er en lommeregner det er skiver i C# sporg ps peter
        //test
        static void Main(string[] args)
        {
            if (Login(out string username) == false)
                Environment.Exit(0);

            do
            {
                Menu(out ConsoleKeyInfo key);
                char Char = key.KeyChar;

                if (Char == '1' || Char == '2' || Char == '3' || Char == '4')
                {
                    string str1 = Console.ReadLine();
                    string str2 = Console.ReadLine();
                    double tal1 = double.Parse(str1);
                    double tal2 = double.Parse(str2);


                    double sum = 0;

                    switch (Char)
                    {

                        case '1':
                            sum = tal1 + tal2;
                            break;
                        case '2':

                            sum = tal1 - tal2;
                            break;
                        case '3':

                            sum = tal1 * tal2;
                            break;
                        case '4':
                            sum = tal1 / tal2;
                            break;
                    };
                    Console.WriteLine("Resultatet er " + sum.ToString());

                    for (int i = 0; i < sum; i++)
                    {
                        Console.Write("*");
                    }
                }
                else
                {
                    Console.WriteLine("didn't work");
                }
            }
            while (Console.ReadKey().Key != ConsoleKey.Escape);

            // asks for credentials and checks if they are correct
            bool Login(out string user)
            {
                //array of username, password pairs
                string[,] logins = { { "Mathgeek", "1234" }, { "1", "1" }, { "admin", "admin" }, { "Anders", "AyyLmao420" }, { "Alex", "MindCrusch!" }, { "PeterTheMenace", "TypicalSkyrimBandit" } };
                for (int i = 0; i < 3; i++)
                {
                    Console.WriteLine("Username: ");
                    user = Console.ReadLine();
                    Console.WriteLine("Password: ");
                    string pass = Console.ReadLine();
                    //checks array for correct username password pair
                    for (int x = 0; x < logins.Length / logins.Rank; x++)
                    {
                        if (user == logins[x, 0] && pass == logins[x, 1])
                        {
                            return true;
                        }
                    }
                    Console.WriteLine("Incorrect login");
                }
                user = "unknown user";
                return false;
            }
            // displays menu and asks for input
            void Menu(out ConsoleKeyInfo key)
            {
                Console.Clear();
                Console.WriteLine("");
                Console.WriteLine("Velkommen " + username);
                Console.WriteLine("-------------------------------");
                Console.WriteLine("1. Læg 2 tal sammen");
                Console.WriteLine("2. Træk 2 tal fra hinanden");
                Console.WriteLine("3. Gang 2 tal med hinanden");
                Console.WriteLine("4. Divider 2 tal med hinanden");
                key = Console.ReadKey(true);
                Console.Clear();
                if (key.Key == ConsoleKey.Escape)
                {
                    Environment.Exit(0);
                }
            }

        }
    }
}
